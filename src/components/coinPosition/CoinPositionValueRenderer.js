import React, {Component} from "react";
import PropTypes from 'prop-types'; 


class CoinPositionValueRenderer extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            value: props.value
        };
    }

    formatValueToString(value) {
        let displayValue = !value ? "" : (value >= 0 ? value.toString() : ("(" + (-value).toString() + ")"));
        return displayValue;
    }
 
    refresh(params) {
        if(params.value !== this.state.value) {
            this.setState({
                value: params.value
            });
        }
        return true;
    }

    render() {
        if (this.state.value > 0){
            return (
                <span>{this.formatValueToString(this.state.value)}</span>
            );
        }
        else{
            return (
                <span className="negative-cell">{this.formatValueToString(this.state.value)}</span>
            );
        }

    }
}
 
CoinPositionValueRenderer.propTypes = {
    value: PropTypes.string
};


export default CoinPositionValueRenderer;