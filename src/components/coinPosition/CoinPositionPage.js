import React from 'react';
import PropTypes from 'prop-types'; 
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux'; 
import CoinPositionGrid from './CoinPositionGrid'; 
import * as positionActions from '../../actions/coinPositionActions';
import LogoutButton from '../session/LogoutButton';
import CheckableSelectInput from '../common/CheckableSelectInput';

class CoinPositionPage extends React.Component { 
      
    constructor(state, context){
        super(state, context); 
        this.refreshPositions = this.refreshPositions.bind(this);
        this.toggleAccountSelection = this.toggleAccountSelection.bind(this);
        this.exportToCSV = this.exportToCSV.bind(this);
        this.grid = React.createRef();
    }
  
    refreshPositions(){
        this.props.actions.loadCoinPositions(this.props.accounts);
    }

    toggleAccountSelection(){ 
        const field = event.target.id;
        const checked = event.target.checked;  
        const account = this.props.accounts.find(a=>a.id == field);
        if (account != null){ 
            this.props.actions.toggleAccount(account, checked);
        }
    }

    exportToCSV(){
        const now = new Date();
        let datestring = now.getFullYear()  + ("0"+(now.getMonth()+1)).slice(-2) + 
        ("0"+(now.getDate()+1)).slice(-2) + "_" + ("0" + now.getHours()).slice(-2) + 
        ("0" + now.getMinutes()).slice(-2) + ("0" + now.getSeconds()).slice(-2); 
        this.grid.current.export("Balance_" + datestring);
    }
    render(){ 
        return (
            <div>
                <h1>Coin Positions</h1>  
                <LogoutButton />
                <div className="header">
                    <CheckableSelectInput items={this.props.accounts} onItemCheckChanged={this.toggleAccountSelection} 
                        itemIdProp="id" itemSelectedProp="selected" itemNameProp="name" description="Accounts"/>

                    <div className="row">
                        
                        <input type="submit"  
                        onClick={this.refreshPositions} className="btn btn-primary" 
                        disabled={this.props.loading}
                        value={this.props.loading ? 'Refreshing...': 'Refresh'}/>
                        <label className="control-label">{this.props.refreshTime}</label>
                        <label className="control-label error-label">{this.props.error}</label>
                        <input type="submit" align="right" onClick={this.exportToCSV} className="btn btn-primary right" 
                            disabled={this.props.loading} value="Export" />
                    </div>
                </div>

                <CoinPositionGrid positions={this.props.positions} ref={this.grid} loading={this.props.loading}/>

            </div>
        );
    }
}
 
CoinPositionPage.propTypes = {
    accounts: PropTypes.array.isRequired,
    positions: PropTypes.array,
    loading: PropTypes.bool,
    refreshTime: PropTypes.string,
    error: PropTypes.string,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
     return {
         accounts: state.accounts,
         positions: state.positions,
         refreshTime: state.refreshTime,
         loading: state.loading,
         error: state.error
     };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(positionActions, dispatch)
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(CoinPositionPage);