import {AgGridReact} from 'ag-grid-react';
import React from 'react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-blue.css';
import { COINS } from '../../api/mockCoinPositionApi';
import CoinPositionValueRenderer from './CoinPositionValueRenderer';
import CoinPositionGridColumnsVisibilityToggler from './CoinPositionGridColumnsVisibilityToggler';
import PropTypes from 'prop-types';
import LoadingOverlay from 'react-loading-overlay';
import Responsive from 'react-responsive';
import storage from '../../utils/storage';

    /*eslint-disable react/no-multi-comp*/ 
const Desktop = props => <Responsive {...props} minWidth={992} />;
const Tablet = props => <Responsive {...props} maxWidth={991.99} />;

class CoinPositionGrid  extends React.Component{

    constructor(props, context){
        super(props, context);
        this.columns = [
            {field:'accountName', headerName: 'Account', resizable: true, sortable: true, pinned: "left"}
        ]; 
        for(let coin in COINS){
            let coinName = COINS[coin];
             
            this.columns.push({field: coinName, headerName: coinName, cellRenderer: "coinPositionValueRenderer", 
            resizable: true, sortable: true, type: "numericColumn" });
        }  

        this.components = {
            coinPositionValueRenderer: CoinPositionValueRenderer
        };  
        this.rowClassRules = {
            "error-row": params => params.data.isError,
            "warning-row" : params => params.data.isWarning
        };
        this.resizeColumns = this.resizeColumns.bind(this);
        this.state ={
            showColumnToggler: false
        };
         
    }


    resizeColumns(params){
        this.gridColumnApi = params.columnApi; 
        this.gridApi = params.api;
        if (!this.state.showColumnToggler){

            this.setState({
                showColumnToggler: true
            });
        }
        let allColumnIds = [];
        for(let coin in COINS){
            let coinName = COINS[coin];
            allColumnIds.push(coinName);
        }  
        this.gridColumnApi.autoSizeColumns(allColumnIds);
    }
 
    export(fileName) {
        let params = {  
          fileName: fileName,
          columnSeparator: ","
        }; 
        this.gridApi.exportDataAsCsv(params);
    }

    render(){
        return (
            <div  className="ag-theme-blue mainView" >
              <LoadingOverlay
                    active={this.props.loading}
                    spinner
                    text="Loading Positions..."
                    >  
                       <Desktop>
                       <AgGridReact 
                            columnDefs={this.columns}
                            rowData={this.props.positions}  
                            frameworkComponents={this.components}  
                            onGridReady={this.resizeColumns} 
                            onBodyScroll={this.resizeColumns}
                            rowClassRules={this.rowClassRules}  
                            />
                       </Desktop>
                       <Tablet>
                       <AgGridReact 
                            columnDefs={this.columns}
                            rowData={this.props.positions}  
                            frameworkComponents={this.components}  
                            onGridReady={this.resizeColumns} 
                            onBodyScroll={this.resizeColumns}
                            rowClassRules={this.rowClassRules} 
                            headerHeight="40"
                            rowHeight="40"
                            />
                       </Tablet>

                </LoadingOverlay>
                {
                    this.state.showColumnToggler && <CoinPositionGridColumnsVisibilityToggler gridColumnApi={this.gridColumnApi} />
                }
            </div>
        );
    }
}

CoinPositionGrid.propTypes = {
    positions: PropTypes.array,
    loading: PropTypes.bool.isRequired
};

export default CoinPositionGrid;