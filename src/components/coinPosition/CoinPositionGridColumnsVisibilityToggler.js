import {AgGridReact} from 'ag-grid-react';
import React from 'react';
import PropTypes from 'prop-types';
import CheckableSelectInput from '../common/CheckableSelectInput';

class CoinPositionGridColumnsVisibilityToggler extends React.Component{
    constructor(props, context){
        super(props, context);
      
        
        this.gridColumnApi = props.gridColumnApi; 
        this.state = {
            columns: this.getColumns()
        };
        this.toggleColumnVisibility = this.toggleColumnVisibility.bind(this); 
    } 
    toggleColumnVisibility(){ 
        const field = event.target.id; 
        const checked = event.target.checked;   
        this.gridColumnApi.setColumnVisible(field, checked);
        this.setState({
            columns: this.getColumns()
        });
    }

    getColumns(){
        let columns = this.gridColumnApi.getAllColumns().filter(col=>col.colId !== "accountName").map(col=>
            {
                return {
                id: col.colId,
                visible: col.visible,
                headerName: col.colDef.headerName};
            }
        ); 
        return columns;
    }
 
    render(){
        
        return (
            <CheckableSelectInput items={this.state.columns} itemIdProp="id" itemNameProp="headerName" itemSelectedProp="visible"
            onItemCheckChanged={this.toggleColumnVisibility} description="Coins"/>
        );
    }

}

CoinPositionGridColumnsVisibilityToggler.propTypes = {
    gridColumnApi: PropTypes.object
};


export default CoinPositionGridColumnsVisibilityToggler;