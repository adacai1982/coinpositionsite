import React from 'react';   
import PropTypes from 'prop-types';
import LoginComponent from './LoginComponent';
import * as sessionActions from '../../actions/sessionActions';
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import {LOGIN_FORM_STATUS} from '../../utils/consts'; 
class LoginPage extends React.Component{
    constructor(props, context){
        super(props, context);
        this.login = this.login.bind(this);
        this.handleValueChanged = this.handleValueChanged.bind(this);
        this.state = {
            userName:'',
            password:'' ,
            
        };
    }

    login(){
       this.props.actions.login(this.state.userName, this.state.password);
    }

    handleValueChanged(){
        const field = event.target.name; 
        const value = event.target.value;
        let newState = {...this.state};
        newState[field] = value;
        this.setState(newState);
    }

    render() {
       return <LoginComponent title="Coin Positions" 
        error={this.props.loginFormStatus == LOGIN_FORM_STATUS.LOGINFAILED ? "Invalid user/password" : null} 
        handleLogin={this.login} handleChange={this.handleValueChanged} 
        userName={this.state.userName}
        password={this.state.password}
        logining={this.props.loginFormStatus == LOGIN_FORM_STATUS.LOGINING}
        />;
    }
}
 
LoginPage.propTypes = {
    loginFormStatus: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        loginFormStatus: state.loginFormStatus 
    };
}

function mapDispatchToProps(dispatch) {
   return {
       actions: bindActionCreators(sessionActions, dispatch)
   };
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);