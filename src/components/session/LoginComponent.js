import React from 'react';   
import PropTypes from 'prop-types'; 
import './styles.css';

const LoginComponent = ({title, userName, password, handleChange, handleLogin, logining, error}) => {
  return (
    <div className="outer">
      <div className="middle">
          <div className="form-signin"> 
          <h1 className="h3 mb-3 font-weight-normal">{title}</h1>
          <div className="form-label-group"> 
            <label htmlFor="inputUser" className="sr-only">User</label>
            <input type="text" id="inputUser" name="userName" className="form-control login-control" placeholder="User" onChange={handleChange} 
            value={userName} required autoFocus/>
          </div>
          
          <div className="form-label-group"> 
            <label htmlFor="inputPassword" className="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" className="form-control login-control" placeholder="Password"  value={password}  onChange={handleChange} required/>
          </div>
          {error && <p className="mb-3 text-center error-label">{error}</p>}
          <button className="btn btn-lg btn-primary btn-block login-control" type="submit" onClick={handleLogin} 
          disabled={logining || userName == '' || password == ''}>{logining ? "Log in..." : "Log in"}</button>
        </div>
      </div>
    </div>

  );
};

LoginComponent.propTypes = {
  title: PropTypes.string.isRequired,
  error: PropTypes.string,
  userName: PropTypes.string,
  password: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  handleLogin: PropTypes.func.isRequired,
  logining: PropTypes.bool.isRequired
};

export default LoginComponent;