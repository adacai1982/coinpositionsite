import {  withRouter } from 'react-router-dom';
import * as sessionActions from '../../actions/sessionActions';
import React from 'react';
import auth from '../../utils/storage';

const LogoutButton = (props) => {
   if (auth.getUserInfo() !== null){
     return   <button type="button" onClick={sessionActions.logout}  className="btn btn-link right top" >Log out</button>;
    } else { 
      return <p/>;
    }
  };

  export default LogoutButton;