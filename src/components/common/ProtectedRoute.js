import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import auth from '../../utils/storage';
 
const ProtectedRoute = ({ component: Component, ...rest }) => 
{
  return ( 
    /*eslint-disable react/jsx-no-bind*/ 
    /*eslint-disable react/prop-types*/
      <Route {...rest} render={props => { 
        if (auth.getUserInfo() !== null){
          return <Component {...props} />;
        } else {
         return <Redirect to={{
            pathname: 'Login',
            state: { from: props.location }
            }}
          />;
        }
         
      }} />
    );
};

ProtectedRoute.propTypes = {
    component: PropTypes.func.isRequired
};


export default  ProtectedRoute;
