import React from 'react';
import PropTypes from 'prop-types';
import Responsive from 'react-responsive';

    /*eslint-disable react/no-multi-comp*/ 
const Desktop = props => <Responsive {...props} minWidth={992} />;
const Tablet = props => <Responsive {...props} maxWidth={991.99} />;


const ToggleSwitch = ({id, name, checked, onCheckChanged}) =>{
    
    return (
        <div key={id + "_container"} className="form-check checkbox-container">
           <Desktop>  
              <div key={id + "_container"} className="form-check checkbox-container">
                <label className="form-check-label"> 
                        <input type="checkbox" key={id} checked={checked} onChange={onCheckChanged} id={id} className="form-check-input"/>
                    {name}
                    </label> 
                </div>
            </Desktop>
            <Tablet>
                <input type="checkbox" key={id} checked={checked} onChange={onCheckChanged} id={id} className="toggleCheckbox"/>
                <label htmlFor={id} className="toggleLabel" />
                <label className="control-label">{name}</label>
                
            </Tablet>
            
        </div>
    );
};


ToggleSwitch.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    checked: PropTypes.bool.isRequired,
    onCheckChanged: PropTypes.func.isRequired
};

export default ToggleSwitch;