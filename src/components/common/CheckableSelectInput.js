import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';
import ToggleSwitch from './ToggleSwitch';
import Responsive from 'react-responsive';

/*eslint-disable react/no-multi-comp*/ 
const Desktop = props => <Responsive {...props} minWidth={992} />;
const Tablet = props => <Responsive {...props} maxWidth={991.99} />;


const CheckableSelectInput = ({description, items, itemIdProp, itemSelectedProp, itemNameProp, onItemCheckChanged}) =>{
     
     return (
        <div>
            <Desktop> 
               <div className="row"> 
                              {items.map((o, i)=>
                                    (<div key={o.id + "_container"} className="form-check checkbox-container">
                                          <ToggleSwitch id={o[itemIdProp]} checked={o[itemSelectedProp]} onCheckChanged={onItemCheckChanged} name={o[itemNameProp]}/> 
                                 </div>)
                              )
                              }
               </div>
            </Desktop>
            <Tablet>
            <details>
               <summary>{description}</summary>
                  <div className="row"> 
                              {items.map((o, i)=>
                                    (<div key={o.id + "_container"} className="form-check checkbox-container">
                                          <ToggleSwitch id={o[itemIdProp]} checked={o[itemSelectedProp]} onCheckChanged={onItemCheckChanged} name={o[itemNameProp]}/> 
                                 </div>)
                              )
                              }
                        </div>
            </details>
            </Tablet>
        </div>
        
        
     );
};

 CheckableSelectInput.propTypes = {
     description: PropTypes.string.isRequired,
     items: PropTypes.array.isRequired,
     itemIdProp: PropTypes.string.isRequired,
     itemSelectedProp: PropTypes.string.isRequired,
     itemNameProp: PropTypes.string.isRequired,
     onItemCheckChanged: PropTypes.func.isRequired
 };

export default CheckableSelectInput;