import delay from './delay'; 

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.

export const COINS = {
    BTC_SWAP: "BTC-SWAP",
    BTC:"BTC",
    ADA:"ADA",
    ADAH19: "ADAH19",
    BCH:"BCH",
    BCHH19:"BCHH19",
    BNB:"BNB",
    EOS:"EOS",
    EOSH19:"EOSH19", 
    ETH:"ETH",
    ETHH19:"ETHH19",
    EUR:"EUR",
    HUSD:"HUSD",
    LTC:"LTC",
    LTCH19: "LTCH19",
    PAX:"PAX",
    TRX:"TRX",
    TRXH19: "TRXH19",
    TUSD:"TUSD",
    USD:"USD",
    USDT: "USDT",
    USDC: "USDC",
    XBTH19: "XBTH19",
    XBTM19: "XBTM19",
    XRP:"XRP",
    XRPH19:"XRPH19"
};

class CoinPositionApi {
 
  static getCoinPositions(accounts) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {  
 
            let shouldMissingAccount = Math.random() < 0.8;

            accounts = accounts.filter(a=>a.selected);
            if (shouldMissingAccount && accounts.length > 2){ 
                
                accounts = Math.random() < 0.5 ? 
                accounts.splice(1, accounts.length - 1) : 
                accounts.splice(0, accounts.length - 2);
            }
            let coinPositions = accounts.map(a=>{

                let position = {
                    accountName: a.name,
                    isError: false,
                    isWarning: false
                };
                for(let coin in COINS){
                    let coinName = COINS[coin];
                    position[coinName] = ((Math.random() * 10000 + Math.random() * 20) * (Math.random() > 0.5 ? 1 : -1)).toFixed(2);
                }
                return position;
            });

            resolve(coinPositions);

        }, delay);
    });
  }

  
}

export default CoinPositionApi;