export const LOGIN_FORM_STATUS = {
    LOGINING: "LOGINING", 
    LOGINED: "LOGINED",
    LOGOUTED: "LOGOUTED",
    LOGINFAILED: "LOGINFAILED"
};
