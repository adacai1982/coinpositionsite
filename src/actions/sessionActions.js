import * as types from './actionTypes';
import sessionApi from '../api/mockSessionApi';  
import {LOGIN_FORM_STATUS} from '../utils/consts';
import auth from '../utils/storage'; 
import { history } from '../index';

export function updateLoginStatus(status){
   
    return {type:types.UPDATE_LOGIN_STATUS, status};
}
   
export function login(userName, password, rememberMe) {

    return function(dispatch){ 

        dispatch(updateLoginStatus(LOGIN_FORM_STATUS.LOGINING));
        return sessionApi.login(userName, password).then(success=>{  
            dispatch(updateLoginStatus(success ? LOGIN_FORM_STATUS.LOGINED: LOGIN_FORM_STATUS.LOGINFAILED)); 

            if (success){
                auth.setUserInfo(userName, rememberMe);
                history.push("/");
            } 

        }).catch(error => {
            throw(error);
        });
    };
}

 export function logout(){
    auth.clearAppStorage();
    history.push("/Login");
 }
 
