import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers'; 
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';

export default function configureStore(initialStore, history){
    return createStore(
        rootReducer(history),
        initialStore,
        applyMiddleware(thunk, routerMiddleware(history))
    );
}