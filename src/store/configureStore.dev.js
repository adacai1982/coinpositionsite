import { createStore, applyMiddleware, compose  } from 'redux';
import rootReducer from '../reducers';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore(initialStore, history){
    return createStore(
        rootReducer(history),
        initialStore,
        composeEnhancers(applyMiddleware(thunk, routerMiddleware(history), reduxImmutableStateInvariant()))
    );
}