import * as types from '../actions/actionTypes';
import initialState from './initialState';


export default function coinPositionReducer(state =initialState.loading, action){
    switch(action.type){
        case types.LOAD_COIN_POSITIONS_SUCCESS:
            return false;  
        case types.LOADING_COIN_POSITIONS:
            return true;
        default:
            return state;
    }
}