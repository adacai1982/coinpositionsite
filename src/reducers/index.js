import { combineReducers} from 'redux';
import positions from './coinPositionReducer'; 
import accounts from './accountReducer'; 
import refreshTime from './refreshTimeReducer'; 
import loading from './loadingReducer'; 
import error from './errorReducer';
import loginFormStatus from './loginFormStatusReducer';  
import { connectRouter } from 'connected-react-router';
 
const rootReducer = (history) => {
    return combineReducers({
        router: connectRouter(history),
        accounts,
        positions,
        refreshTime,
        loading,
        error,
        loginFormStatus 
    });    
};

export default rootReducer;