 import {LOGIN_FORM_STATUS} from '../utils/consts';

export default {
    accounts:[], 
    positions:[],
    refreshTime:'',
    loading: false,
    error: '',
    loginFormStatus: LOGIN_FORM_STATUS.LOGOUTED 
};