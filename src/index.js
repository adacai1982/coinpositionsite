import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Switch, Route } from 'react-router-dom'; 
import configureStore from './store/configureStore';
import { Provider } from 'react-redux';
import './styles/styles.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'; 
import { loadAccounts } from './actions/accountActions'; 
import CoinPositionPage from './components/coinPosition/CoinPositionPage'; 
import LoginPage from './components/session/LoginPage';
import initialState from './reducers/initialState';
import ProtectedRoute from './components/common/ProtectedRoute'; 
import createHistory from 'history/createBrowserHistory';


const history = createHistory(); 
const store = configureStore(initialState, history);
store.dispatch(loadAccounts(true));


render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
          <div>
            <Switch>
               <Route exact path="/Login" component={LoginPage} /> 
               <ProtectedRoute component={CoinPositionPage} /> 
            </Switch>
          </div>
        </ConnectedRouter> 
 
    </Provider>,
    document.getElementById('app')
);

export { history };